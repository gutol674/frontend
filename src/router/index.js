import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

//Product
const Products = () => import("../views/Products");
const ShowProduct = () => import("../views/ShowProduct");
const CreateProduct = () => import("../views/CreateProduct");

//Order
const Orders = () => import("../views/Orders");
const ShowOrder = () => import("../views/ShowOrder");
const CreateOrder = () => import("../views/CreateOrder");

//Quotation
const Quotation = () => import("../views/Quotation");

const routes = [
    {
        path: "/products",
        name: "Products",
        component: Products
    },
    {
        path: "/products/:id",
        name: "Show Product",
        component: ShowProduct
    },
    {
        path: "/products/new",
        name: "Create Product",
        component: CreateProduct
    },
    {
        path: "/orders",
        name: "Orders",
        component: Orders
    },
    {
        path: "/orders/new",
        name: "Create Order",
        component: CreateOrder
    },
    {
        path: "/orders/:id",
        name: "Show Order",
        component: ShowOrder
    },
    {
        path: "/quotation",
        name: "Quotation",
        component: Quotation
    },
];

const router = new VueRouter({
    routes
});

export default router;